# ics-ans-role-elk-stack

Ansible role to install elk-stack.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-elk-stack
```

## License

BSD 2-clause
